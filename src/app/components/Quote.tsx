'use client';

import {IQuote} from "../../../types/quotes";
import React, {FormEventHandler} from "react";
import { FiEdit } from "react-icons/fi";

import { FaRegTrashAlt } from "react-icons/fa";
import Modal from "@/app/components/Modal";
import {addQuote, deleteQuote, editQuote} from "../../../api";
import {useRouter} from "next/navigation";
import {v4 as uuidv4} from "uuid";


interface QuoteProps {
    quote: IQuote
}
const Quote: React.FC<QuoteProps> = ({quote}) => {
    const [modalEditOpen, setEditModalOpen] = React.useState(false);
    const [modalDeleteOpen, setDeleteModalOpen] = React.useState(false);
    const [quoteToEdit, setQuoteToEdit] = React.useState(quote.text);
    const router = useRouter();

    const handleSubmitEditQuote: FormEventHandler<HTMLFormElement> = async (e) => {
        e.preventDefault();
        console.log(quote)
        await editQuote({
            id: quote.id,
            author: quote.author,
            text: quoteToEdit,
            priority: quote.priority
        });
        setEditModalOpen(false);
        router.refresh();
    };

    const handleDeleteQuote: (id: string) => Promise<void> = async (id: string) => {
        console.log(quote)
        await deleteQuote(quote.id);
        setDeleteModalOpen(false);
        router.refresh();
    }

    return <tr key={quote.id}>
        <td>{quote.priority}</td>
        <td>{quote.author}</td>
        <td>{quote.text}</td>
        <td className="flex gap-5">
            <FiEdit onClick={() => setEditModalOpen(true)} cursor="pointer" size={25} className="text-green-950" />
            <Modal modalOpen={modalEditOpen} setModalOpen={setEditModalOpen}>
                <form onSubmit={handleSubmitEditQuote}>
                    <h3 className="font-bold text-lg text-center">Edit quote</h3>
                    <h5 className="font-bold text-amber-800 text-left ">{quote.author}</h5>
                    <div className="modal-action flex space-x-4">
                        <input type="text" placeholder="Type here" className="input input-bordered input-primary input-ghost w-full" value={quoteToEdit} onChange={e => setQuoteToEdit(e.target.value)}/>
                        <button type="submit" className="btn btn-primary ">Edit</button>
                    </div>
                </form>
            </Modal>
            <FaRegTrashAlt onClick={() => setDeleteModalOpen(true)} cursor="pointer" size={25} className="text-red-950"/>
            <Modal modalOpen={modalDeleteOpen} setModalOpen={setDeleteModalOpen}>
                <h3 className="font-bold text-lg text-center pb-5">Delete quote ?</h3>
                    <button onClick={() => handleDeleteQuote(quote.id)} type="submit" className="btn btn-warning w-full" >Delete</button>
            </Modal>
        </td>
    </tr>;
};

export default Quote;