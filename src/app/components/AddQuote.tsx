'use client';

import { FaPlus } from "react-icons/fa6";
import React, {FormEventHandler} from "react";
import Modal from "@/app/components/Modal";
import {addQuote} from "../../../api";
import {useRouter} from "next/navigation";
import { v4 as uuidv4 } from 'uuid';

const AddQuote = () => {
    const router = useRouter();
    const [modalOpen, setModalOpen] = React.useState(false);
    const [quote, setQuote] = React.useState("");
    const [author, setAuthor] = React.useState("");
    const handleSubmitNewQuote: FormEventHandler<HTMLFormElement> = async (e) => {
        e.preventDefault();
        console.log(quote)
        await addQuote({
            id: uuidv4(),
            author: author,
            text: quote,
            priority: "1"
        });
        setModalOpen(false);
        setQuote("");
        router.refresh();
    };

    return <div>
        <button onClick={() => setModalOpen(true)} className="btn btn-primary w-full">Add quote
            <FaPlus/>
        </button>

        <Modal modalOpen={modalOpen} setModalOpen={setModalOpen}>
            <form onSubmit={handleSubmitNewQuote}>
                <h3 className="font-bold text-lg">Add new quote</h3>
                <input type="text" placeholder="Author's name"
                       className="input input-bordered input-primary input-ghost w-1/2" value={author}
                       onChange={e => setAuthor(e.target.value)}/>
                <div className="modal-action flex flex-col space-y-4">
            <textarea placeholder="Type quote here"
                      className="input input-bordered input-primary input-ghost w-full h-20" value={quote}
                      onChange={e => setQuote(e.target.value)}/>
                    <button type="submit" className="btn btn-primary ">Add</button>
                </div>
            </form>
        </Modal>
    </div>;
};

export default AddQuote;