import {IQuote} from "../../../types/quotes";
import Quote from "@/app/components/Quote";

interface QuotesListProps {
    quotes: IQuote[]
}

const QuotesList: React.FC<QuotesListProps> = ({quotes}) => {
    return <div className="overflow-x-auto">
        <table className="table">
            {/* head */}
            <thead>
            <tr>
                <th>Priority</th>
                <th>Author</th>
                <th>Quotes</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            {quotes.map((quote) => <Quote quote={quote} key={quote.id}/>)}
            </tbody>
        </table>
    </div>
};

export default QuotesList;