import AddQuote from "@/app/components/AddQuote";
import QuotesList from "@/app/components/QuotesList";
import {getAllQuotes} from "../../api";

export default async function Home() {
    const quotes = await getAllQuotes();

    return (
        <main className="flex min-h-screen flex-col max-w-4xl mx-auto">
            <div className="flex flex-col text-center my-5 gap-4">
                <h1 className="text-2xl font-bold">Quotes</h1>
                <AddQuote />
            </div>
            <QuotesList quotes={quotes}/>
        </main>
    );
}
