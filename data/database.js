const sqlite3 = require('sqlite3').verbose();

// Open a database handle
const db = new sqlite3.Database('./quotes.db', sqlite3.OPEN_READWRITE | sqlite3.OPEN_CREATE, (err) => {
    if (err) {
        console.error(err.message);
    }
    console.log('Connected to the quotes database.');
});

db.serialize(() => {
    db.run(`
    CREATE TABLE IF NOT EXISTS quotes(
      id TEXT PRIMARY KEY,
      author TEXT,
      text TEXT,
      priority INTEGER
    )
  `, (err) => {
        if (err) {
            console.error(err.message);
        }
        console.log('Quotes table created successfully.');
    });
});

module.exports = db;