import {IQuote} from "./types/quotes";

const baseUrl = "http://localhost:3001";

export const getAllQuotes = async (): Promise<IQuote[]> => {
    const res = await fetch(`${baseUrl}/quotes`, { cache: 'no-store' });
    return await res.json();
}

export const addQuote = async (quote: { id: string; author: string, text: string; priority: string }): Promise<IQuote> => {
    const res = await fetch(`${baseUrl}/quotes`, {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(quote)
    });
    return await res.json();
}

export const editQuote = async (quote: { id: string; author: string; text: string; priority: string }): Promise<IQuote> => {
    const res = await fetch(`${baseUrl}/quotes/${quote.id}`, {
        method: "PUT",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(quote)
    });
    return await res.json();
}

export const deleteQuote = async (id: string): Promise<void> => {
    await fetch(`${baseUrl}/quotes/${id}`, {
        method: "DELETE"
    });
}
