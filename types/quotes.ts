export interface IQuote {
    id: string,
    author: string,
    text: string,
    priority: string
}